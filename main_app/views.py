import math
from scipy.stats import norm

from django.shortcuts import render
from django.http import HttpResponse, JsonResponse


def calculator(request):
	return render(request, 'calculator.html')


def get_significance(request):
	if request.method == 'POST':
		control_visitors = int(request.POST.get('control_visitors', None))
		variation_visitors = int(request.POST.get('variation_visitors', None))
		control_conversions = int(request.POST.get('control_conversions', None))
		variation_conversions = int(request.POST.get('variation_conversions', None))
		significance = 'No'

		try:
			control_p = control_conversions / control_visitors
			variation_p = variation_conversions / variation_visitors

			control_se = math.sqrt((control_p * ( 1 - control_p)) / control_visitors)
			variation_se = math.sqrt((variation_p * (1 - variation_p)) / variation_visitors)

			z_score = (control_p - variation_p) / math.sqrt((control_se**2)+(variation_se**2))
			p_value = round(norm.cdf(z_score), 3)

			# 90% confidence
			if p_value < 0.1 or p_value > 0.9: 
				significance = 'Yes!'

			if z_score > 0:
				p_value = round(1 - norm.cdf(z_score), 3)
		except Exception as e:
			print('%s' % e)
			p_value = 'NaN'

		return JsonResponse({"p_value": p_value, 'significance': significance })
	else:
		return HttpResponse('Invalid access', status=405)
